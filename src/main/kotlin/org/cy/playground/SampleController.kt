package org.cy.playground

import org.springframework.web.bind.annotation.*

data class Message(
        val id: Long,
        val message: String
)

@RestController
@RequestMapping("/sample")
class SampleController {

    @GetMapping("/hello")
    fun getHello(): String {
        return "Hello"
    }

    @GetMapping("/path-variable/{param}")
    fun getPathVariable(@PathVariable("param") param: String): String {
        return "Path variable is $param"
    }

    @GetMapping("/query-param")
    fun getQueryParam(@RequestParam param: String): String {
        return "Query string parameter with name 'param' is $param"
    }

    @PostMapping("/post-message")
    fun postMessage(@RequestBody message: Message): String {
        return "ID is ${message.id} and message is ${message.message}"
    }

    @PutMapping("/put-message/{id}")
    fun putMessage(@PathVariable("id") id: Long, @RequestBody message: Message): Message {
        return Message(id, message.message)
    }
}
