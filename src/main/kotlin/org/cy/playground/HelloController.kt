package org.cy.playground

import org.springframework.web.bind.annotation.*

@RestController
class HelloController {

    @RequestMapping("/")
    internal fun home(): String {
        return "Hello World from Spring webmvc !"
    }
}