package org.cy.playground

import org.cy.playground.db.Tables.AUTHORS
import org.cy.playground.db.Tables.MESSAGES
import org.jooq.impl.DefaultDSLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

data class MessageAndAuthor(
        val messageId: Int,
        val message: String,
        val authorId: Int,
        val authorUsername: String
)

@RestController
class MessagesController {

    @Autowired
    lateinit var dsl: DefaultDSLContext

    @RequestMapping("/messages")
    internal fun home(): Iterable<MessageAndAuthor> {
        return dsl
                .select()
                .from(MESSAGES.join(AUTHORS).on(MESSAGES.AUTHOR_ID.eq(AUTHORS.ID)))
                .fetch()
                .map({record -> MessageAndAuthor(record[MESSAGES.ID], record[MESSAGES.MESSAGE], record[MESSAGES.AUTHOR_ID], record[AUTHORS.USERNAME])})
    }
}