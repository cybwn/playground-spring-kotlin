package org.cy.playground

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.JWTVerificationException
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import java.io.IOException
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

internal data class UserFromPayload(
        val username: String
)

class AuthFilter(
        authenticationManager: AuthenticationManager,
        private val json: ObjectMapper
) : AbstractAuthenticationProcessingFilter(AntPathRequestMatcher("/secured/**")) {

    init {
        setAuthenticationManager(authenticationManager)
        setAuthenticationFailureHandler { request, response, exception ->
            SecurityContextHolder.clearContext()
            response.status = HttpStatus.UNAUTHORIZED.value()
        }
    }

    @Throws(AuthenticationException::class)
    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication {
        val header = request.getHeader("Authorization")

        if (header == null || !header.startsWith("Bearer ")) {
            throw AuthenticationCredentialsNotFoundException("Authorization header not found, or not starting by Bearer")
        }

        val authToken = header.substring(7)

        try {
            val algorithm = Algorithm.HMAC256("secret")
            val verifier = JWT.require(algorithm)
                    .withIssuer("Online JWT Builder")
                    .build()
            val jwt = verifier.verify(authToken)
            val userFromPayload = json.readValue(Base64.getDecoder().decode(jwt.payload), UserFromPayload::class.java)
            val usernamePasswordAuthenticationToken = UsernamePasswordAuthenticationToken(userFromPayload.username, "", null)
            SecurityContextHolder.getContext().authentication = usernamePasswordAuthenticationToken
            setContinueChainBeforeSuccessfulAuthentication(true)
            return usernamePasswordAuthenticationToken
        } catch (exception: JWTVerificationException) {
            throw AuthenticationCredentialsNotFoundException("Invalid token")
        } catch (exception: IOException) {
            throw AuthenticationCredentialsNotFoundException("Invalid token")
        }
    }
}
