package org.cy.playground

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal

@RestController
@RequestMapping("/secured")
class SecureController {

    @GetMapping("/")
    fun getHello(): String {
        return "this is secured"
    }

    @GetMapping("/whoami")
    fun getWhoami(principal: Principal): String {
        return "this is secured: " + principal.name
    }
}
