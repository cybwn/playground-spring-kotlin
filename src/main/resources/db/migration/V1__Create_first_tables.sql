CREATE TABLE authors (
    id SERIAL PRIMARY KEY,
    username character varying
);

CREATE TABLE messages (
    id SERIAL PRIMARY KEY,
    message character varying,
    author_id integer REFERENCES authors (id)
);

INSERT INTO authors (id, username) VALUES
(1, 'cybwn');

INSERT INTO messages (id, message, author_id) VALUES
(1, 'Hello from postgres', 1),
(2, 'AAA', 1);
